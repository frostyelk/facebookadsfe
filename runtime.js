/**
 Copyright 2015 Frosty Elk AB
 */

// ECMAScript 5 strict mode
"use strict";

assert2(cr, "cr namespace not created");
assert2(cr.plugins_, "cr.plugins_ not created");

/////////////////////////////////////
// Plugin class
cr.plugins_.FacebookAdsFE = function (runtime) {
    this.runtime = runtime;
};

(function () {

    var pluginProto = cr.plugins_.FacebookAdsFE.prototype;

    /////////////////////////////////////
    // Object type class
    pluginProto.Type = function (plugin) {
        this.plugin = plugin;
        this.runtime = plugin.runtime;
    };

    var typeProto = pluginProto.Type.prototype;

    // Plugin common vars
    var fbAdsReady = false;
    var isTest = true;
    var isAutoShow = false;
    var conditionTag = {};
    var fbAdsInst = {};
    var fbAdsRuntime = {};
    var adUnitId = "";
    var fbAdsOptions = {};
    var lastError = "";

    var deviceHash = "";

    function isAndroid() {
        return /android|tizen/i.test(navigator.userAgent);
    }

    function isIOS() {
        return /iphone|ipad|ipod/i.test(navigator.userAgent);
    }

    // Banner sizes for https://github.com/floatinghotpot/cordova-plugin-facebookads/blob/master/www/FacebookAds.js
    var fbAdsBannerSize = [
        'SMART_BANNER',
        'BANNER'
    ];

    var fbAdsBannerPosition = 0;

    // called on startup for each object type
    typeProto.onCreate = function () {

    };

    /////////////////////////////////////
    // Instance class
    pluginProto.Instance = function (type) {
        this.type = type;
        this.runtime = type.runtime;
    };

    var instanceProto = pluginProto.Instance.prototype;

    // called whenever an instance is created

    instanceProto.onCreate = function () {

        if (typeof cordova === 'undefined') {
            console.log("FacebookAdsFE passive, not running in Cordova");
            return;
        }

        if (typeof ["FacebookAds"] !== 'undefined') {
            console.log("FacebookAdsFE running with Cordova plugin FacebookAds");
            fbAdsReady = true;
        } else {
            console.log("FacebookAdsFE not created, a supported Cordova Facebook Ads plugin is missing");
            return;
        }

        fbAdsInst = this;
        fbAdsRuntime = this.runtime;

        isTest = this.properties[0] === 0;
        isAutoShow = this.properties[1] === 0;
        deviceHash = this.properties[2];

        // deviceHash = this.properties[3];


        // Setup listeners
        document.addEventListener('onAdLoaded', function (data) {
            console.log("onAdLoaded");
            console.dir(data);

            if (data["adType"] === 'interstitial') {
                fbAdsRuntime.trigger(cr.plugins_.FacebookAdsFE.prototype.cnds.onInterstitialReceive, fbAdsInst);
            } else {
                fbAdsRuntime.trigger(cr.plugins_.FacebookAdsFE.prototype.cnds.onBannerReceive, fbAdsInst);
            }
        });

        document.addEventListener('onAdFailLoad', function (data) {
            console.log("onAdFailLoad");
            console.dir(data);

            if (data["adType"] === 'interstitial') {
                fbAdsRuntime.trigger(cr.plugins_.FacebookAdsFE.prototype.cnds.onInterstitialFailedToReceive, fbAdsInst);
            } else {
                fbAdsRuntime.trigger(cr.plugins_.FacebookAdsFE.prototype.cnds.onBannerFailedToReceive, fbAdsInst);
            }

        });

        document.addEventListener('onAdPresent', function (data) {
            console.log("onPresentAd");
            console.dir(data);

            if (data["adType"] === 'interstitial') {
                fbAdsRuntime.trigger(cr.plugins_.FacebookAdsFE.prototype.cnds.onInterstitialPresent, fbAdsInst);
            } else {
                fbAdsRuntime.trigger(cr.plugins_.FacebookAdsFE.prototype.cnds.onBannerPresent, fbAdsInst);
            }


        });

        document.addEventListener('onAdDismiss', function (data) {
            console.log("onAdDismiss");
            console.dir(data);

            if (data["adType"] === 'interstitial') {
                fbAdsRuntime.trigger(cr.plugins_.FacebookAdsFE.prototype.cnds.onInterstitialDismiss, fbAdsInst);
            } else {
                fbAdsRuntime.trigger(cr.plugins_.FacebookAdsFE.prototype.cnds.onBannerDismiss, fbAdsInst);
            }
        });

        document.addEventListener('onAdLeaveApp', function (data) {
            console.log("onAdLeaveApp");
            console.dir(data);

            if (data["adType"] === 'interstitial') {
                fbAdsRuntime.trigger(cr.plugins_.FacebookAdsFE.prototype.cnds.onInterstitialLeaveApp, fbAdsInst);
            } else {
                fbAdsRuntime.trigger(cr.plugins_.FacebookAdsFE.prototype.cnds.onBannerLeaveApp, fbAdsInst);
            }
        });

    };

    // The comments around these functions ensure they are removed when exporting, since the
    // debugger code is no longer relevant after publishing.
    /**BEGIN-PREVIEWONLY**/
    instanceProto.getDebuggerValues = function (propsections) {
        // Append to propsections any debugger sections you want to appear.
        // Each section is an object with two members: "title" and "properties".
        // "properties" is an array of individual debugger properties to display
        // with their name and value, and some other optional settings.
        propsections.push({
            "title": "My debugger section",
            "properties": [
                // Each property entry can use the following values:
                // "name" (required): name of the property (must be unique within this section)
                // "value" (required): a boolean, number or string for the value
                // "html" (optional, default false): set to true to interpret the name and value
                //									 as HTML strings rather than simple plain text
                // "readonly" (optional, default false): set to true to disable editing the property

                // Example:
                // {"name": "My property", "value": this.myValue}
            ]
        });
    };

    instanceProto.onDebugValueEdited = function (header, name, value) {
        // Called when a non-readonly property has been edited in the debugger. Usually you only
        // will need 'name' (the property name) and 'value', but you can also use 'header' (the
        // header title for the section) to distinguish properties with the same name.
        if (name === "My property")
            this.myProperty = value;
    };
    /**END-PREVIEWONLY**/

    //////////////////////////////////////
    // Conditions
    function Cnds() {
    }

    Cnds.prototype.IsReady = function () {
        return fbAdsReady;
    };

    Cnds.prototype.onCreateFullScreenAdSuccessful = function (tag) {
        return cr.equals_nocase(tag, conditionTag.create);
    };


    Cnds.prototype.onCreateFullScreenAdFailed = function (tag) {
        return cr.equals_nocase(tag, conditionTag.create);
    };


    Cnds.prototype.onShowFullScreenAdSuccessful = function (tag) {
        return cr.equals_nocase(tag, conditionTag.show);
    };


    Cnds.prototype.onShowFullScreenAdFailed = function (tag) {
        return cr.equals_nocase(tag, conditionTag.show);
    };


    Cnds.prototype.onCreateBannerAdSuccessful = function (tag) {
        return cr.equals_nocase(tag, conditionTag.banner);
    };

    Cnds.prototype.onCreateBannerAdFailed = function (tag) {
        return cr.equals_nocase(tag, conditionTag.banner);
    };

    Cnds.prototype.onShowBannerAdSuccessful = function (tag) {
        return cr.equals_nocase(tag, conditionTag.bannerShow);
    };

    Cnds.prototype.onShowBannerAdFailed = function (tag) {
        return cr.equals_nocase(tag, conditionTag.bannerShow);
    };

    Cnds.prototype.onBannerFailedToReceive = function () {
        return true;
    };
    Cnds.prototype.onBannerReceive = function () {
        return true;
    };
    Cnds.prototype.onBannerPresent = function () {
        return true;
    };
    Cnds.prototype.onBannerLeaveApp = function () {
        return true;
    };
    Cnds.prototype.onBannerDismiss = function () {
        return true;
    };

    Cnds.prototype.onInterstitialFailedToReceive = function () {
        return true;
    };
    Cnds.prototype.onInterstitialReceive = function () {
        return true;
    };
    Cnds.prototype.onInterstitialPresent = function () {
        return true;
    };
    Cnds.prototype.onInterstitialLeaveApp = function () {
        return true;
    };
    Cnds.prototype.onInterstitialDismiss = function () {
        return true;
    };


    pluginProto.cnds = new Cnds();

    //////////////////////////////////////
    // Actions
    function Acts() {
    }


    Acts.prototype.createFullScreenAd = function (tag, adAndroidUnitId, adIOSUnitId) {
        if (!fbAdsReady) return;

        conditionTag.create = "";


        if (isAndroid()) {
            adUnitId = adAndroidUnitId;
        } else if (isIOS()) {
            adUnitId = adIOSUnitId;
        } else {
            conditionTag.create = tag;
            fbAdsRuntime.trigger(cr.plugins_.FacebookAdsFE.prototype.cnds.onCreateFullScreenAdFailed, fbAdsInst);
            return;
        }


        var options = {
            'adId': adUnitId,
            'autoShow': isAutoShow,
            'isTesting': isTest,
            'deviceHash': deviceHash
        };

        console.log("Action createFullScreenAd with options: " + JSON.stringify(options));

        FacebookAds["prepareInterstitial"](options, function (response) {
            console.log("prepareInterstitial successful. respons: " + JSON.stringify(response));

            conditionTag.create = tag;
            fbAdsRuntime.trigger(cr.plugins_.FacebookAdsFE.prototype.cnds.onCreateFullScreenAdSuccessful, fbAdsInst);

        }, function (respons) {
            lastError = respons;
            console.log("prepareInterstitial failed. respons: " + JSON.stringify(respons));

            conditionTag.create = tag;
            fbAdsRuntime.trigger(cr.plugins_.FacebookAdsFE.prototype.cnds.onCreateFullScreenAdFailed, fbAdsInst);
        });

    };


    Acts.prototype.showFullScreenAd = function (tag) {
        if (!fbAdsReady) return;

        console.log("Action showFullScreenAd: ");

        conditionTag.show = "";

        try {
            FacebookAds["showInterstitial"]();
            console.log("showInterstitial successful");

            conditionTag.show = tag;
            fbAdsRuntime.trigger(cr.plugins_.FacebookAdsFE.prototype.cnds.onShowFullScreenAdSuccessful, fbAdsInst);

        } catch (error) {
            lastError = error;
            console.log("showInterstitial failed. respons: " + JSON.stringify(error));
            conditionTag.show = tag;
            fbAdsRuntime.trigger(cr.plugins_.FacebookAdsFE.prototype.cnds.onShowFullScreenAdFailed, fbAdsInst);
        }

    };


    Acts.prototype.createBannerAd = function (tag, adAndroidUnitId, adIOSUnitId, adBannerSize, adPositionType, adOverlap) {
        if (!fbAdsReady) return;

        if (isAndroid()) {
            adUnitId = adAndroidUnitId;
        } else if (isIOS()) {
            adUnitId = adIOSUnitId;
        } else {
            conditionTag.banner = tag;
            fbAdsRuntime.trigger(cr.plugins_.FacebookAdsFE.prototype.cnds.onCreateBannerAdFailed, fbAdsInst);
            return;
        }

        var isOverlap = adOverlap !== 0;
        fbAdsBannerPosition = adPositionType;

        fbAdsOptions = {
            'adSize': fbAdsBannerSize[adBannerSize],
            'publisherId': adUnitId,
            'isTesting': isTest,
            'autoShow': isAutoShow,
            'deviceHash': deviceHash,
            'position': adPositionType,
            'overlap': isOverlap
        };

        console.log("Banner Ad options: " + JSON.stringify(fbAdsOptions));

        FacebookAds["createBanner"](fbAdsOptions, function (response) {
            console.log("createBanner successful. respons: " + JSON.stringify(response));
            conditionTag.banner = tag;
            fbAdsRuntime.trigger(cr.plugins_.FacebookAdsFE.prototype.cnds.onCreateBannerAdSuccessful, fbAdsInst);
        }, function (error) {
            lastError = error;
            console.log("createBanner failed. respons: " + JSON.stringify(error));
            conditionTag.banner = tag;
            fbAdsRuntime.trigger(cr.plugins_.FacebookAdsFE.prototype.cnds.onCreateBannerAdFailed, fbAdsInst);
        });


    };


    Acts.prototype.showBannerAd = function (tag) {
        if (!fbAdsReady) return;

        conditionTag.bannerShow = "";

        var options = fbAdsBannerPosition;

        FacebookAds["showBanner"](options, function (response) {
            conditionTag.bannerShow = tag;
            console.log("showBanner successful: " + JSON.stringify(response));
            fbAdsRuntime.trigger(cr.plugins_.FacebookAdsFE.prototype.cnds.onShowBannerAdSuccessful, fbAdsInst);
        }, function (error) {
            lastError = error;
            conditionTag.bannerShow = tag;
            console.log("showBanner failed: " + JSON.stringify(error));
            fbAdsRuntime.trigger(cr.plugins_.FacebookAdsFE.prototype.cnd.onShowBannerAdFailed, fbAdsInst);
        });

    };


    Acts.prototype.removeBannerAd = function () {
        if (!fbAdsReady) return;

        FacebookAds["removeBanner"](function (response) {
            console.log("removeBanner successful: " + JSON.stringify(response));
        }, function (error) {
            lastError = error;
            console.log("removeBanner failed: " + JSON.stringify(error));
        });
    };


    Acts.prototype.hideBannerAd = function () {
        if (!fbAdsReady) return;


        try {
            FacebookAds["hideBanner"](function (response) {
                console.log("hideBanner successful: " + JSON.stringify(response));
            }, function (error) {
                lastError = error;
                console.log("hideBanner failed: " + JSON.stringify(error));
            });

        } catch (e) {
            lastError = JSON.stringify(e);
            console.log("hideBanner crashed: " + JSON.stringify(e));
        }

    };

    pluginProto.acts = new Acts();

    //////////////////////////////////////
    // Expressions
    function Exps() {
    }

    Exps.prototype.LastError = function (ret) {
        ret.set_string(lastError);
    };


    pluginProto.exps = new Exps();

}
());