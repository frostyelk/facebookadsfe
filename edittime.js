//noinspection JSUnusedGlobalSymbols
/**
 Copyright 2015 Frosty Elk AB
 */

//noinspection JSUnusedGlobalSymbols
function GetPluginSettings() {
    //noinspection NonShortCircuitBooleanExpressionJS
    return {
        "name": "Facebook Ads FE", // as appears in 'insert object' dialog, can be changed as long as "id" stays the same
        "id": "FacebookAdsFE", 	// this is used to identify this plugin and is saved to the project; never change it
        "version": "1.0.0.0", 	// (float in x.y format) Plugin version - C2 shows compatibility warnings based on this
        "description": "Facebook Ads",
        "author": "Frosty Elk AB",
        "help url": "http://www.frostyelk.se",
        "cordova-plugins": "cordova-plugin-facebookads",
        // "dependency":	"xxx.js",
        "category": "Frosty Elk", // Prefer to re-use existing categories, but you can set anything here
        "type": "object", 	// either "world" (appears in layout and is drawn), else "object"
        "rotatable": false, 		// only used when "type" is "world".  Enables an angle property on the object.
        "flags": 0 			// uncomment lines to enable flags...
        | pf_singleglobal		// exists project-wide, e.g. mouse, keyboard.  "type" must be "object".
        //	| pf_texture // object has a single texture (e.g. tiled background)
        //	| pf_position_aces		// compare/set/get x, y...
        //	| pf_size_aces			// compare/set/get width, height...
        //	| pf_angle_aces			// compare/set/get angle (recommended that "rotatable" be set to true)
        //	| pf_appearance_aces	// compare/set/get visible, opacity...
        //	| pf_tiling				// adjusts image editor features to better suit tiled images (e.g. tiled background)
        //	| pf_animations			// enables the animations system.  See 'Sprite' for usage
        //	| pf_zorder_aces		// move to top, bottom, layer...
        //  | pf_nosize				// prevent resizing in the editor
        //	| pf_effects			// allow WebGL shader effects to be added
        //  | pf_predraw			// set for any plugin which draws and is not a sprite (i.e. does not simply draw
        // a single non-tiling image the size of the object) - required for effects to work properly
    };
}

////////////////////////////////////////
// Parameter types:
// AddNumberParam(label, description [, initial_string = "0"])			// a number
// AddStringParam(label, description [, initial_string = "\"\""])		// a string
// AddAnyTypeParam(label, description [, initial_string = "0"])			// accepts either a number or string
// AddCmpParam(label, description)										// combo with equal, not equal, less, etc.
// AddComboParamOption(text)											// (repeat before "AddComboParam" to add combo items)
// AddComboParam(label, description [, initial_selection = 0])			// a dropdown list parameter
// AddObjectParam(label, description)									// a button to click and pick an object type
// AddLayerParam(label, description)									// accepts either a layer number or name (string)
// AddLayoutParam(label, description)									// a dropdown list with all project layouts
// AddKeybParam(label, description)										// a button to click and press a key (returns a VK)
// AddAnimationParam(label, description)								// a string intended to specify an animation name
// AddAudioFileParam(label, description)								// a dropdown list with all imported project audio files

////////////////////////////////////////
// Conditions

// AddCondition(id,					// any positive integer to uniquely identify this condition
//				flags,				// (see docs) cf_none, cf_trigger, cf_fake_trigger, cf_static, cf_not_invertible,
//									// cf_deprecated, cf_incompatible_with_triggers, cf_looping
//				list_name,			// appears in event wizard list
//				category,			// category in event wizard list
//				display_str,		// as appears in event sheet - use {0}, {1} for parameters and also <b></b>, <i></i>
//				description,		// appears in event wizard dialog when selected
//				script_name);		// corresponding runtime function name
AddCondition(0, 0, "Is Ready", "Start up", "Is Ready", "True when the Facebook Ads API has loaded and is ready to be used.", "IsReady");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(1, cf_trigger, "On Create full screen Ad successful", "Interstitial/Full screen Ad Action events", "On Create full screen Ad successful for tag <b>{0}</b>", "Triggered when the full screen Ad has been created.", "onCreateFullScreenAdSuccessful");
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(2, cf_trigger, "On Create full screen Ad failed", "Interstitial/Full screen Ad Action events", "On Create full screen Ad failed for tag <b>{0}</b>", "Triggered if creating the Ad fails.", "onCreateFullScreenAdFailed");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(3, cf_trigger, "On Show full screen Ad successful", "Interstitial/Full screen Ad Action events", "On show full screen Ad successful for tag <b>{0}</b>", "Triggered when the full screen Ad has been showed.", "onShowFullScreenAdSuccessful");
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(4, cf_trigger, "On Show full screen Ad failed", "Interstitial/Full screen Ad Action events", "On show full screen Ad failed for tag <b>{0}</b>", "Triggered if showing the Ad fails.", "onShowFullScreenAdFailed");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(100, cf_trigger, "On Create Banner Ad successful", "Banner Ad Action events", "On Banner Ad create successful for tag <b>{0}</b>", "Triggered if creating the Ad is successful.", "onCreateBannerAdSuccessful");
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(110, cf_trigger, "On Create Banner Ad failed", "Banner Ad Action events", "On Banner Ad create failed for tag <b>{0}</b>", "Triggered if creating the Ad fails.", "onCreateBannerAdFailed");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(120, cf_trigger, "On Show Banner Ad successful", "Banner Ad Action events", "On Banner Ad show successful for tag <b>{0}</b>", "Triggered if showing the Ad is successful.", "onShowBannerAdSuccessful");
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.", "\"\"");
AddCondition(130, cf_trigger, "On Show Banner Ad failed", "Banner Ad Action events", "On Banner Ad show failed for tag <b>{0}</b>", "Triggered if showing the Ad fails.", "onShowBannerAdFailed");

AddCondition(200, cf_trigger, "On Banner failed to receive", "Banner Ad events", "On Banner failed to receive", "On Banner failed to receive.", "onBannerFailedToReceive");
AddCondition(210, cf_trigger, "On Banner receive", "Banner Ad events", "On Banner receive", "On Banner receive.", "onBannerReceive");
AddCondition(220, cf_trigger, "On Banner present", "Banner Ad events", "On Banner present", "On Banner present.", "onBannerPresent");
AddCondition(230, cf_trigger, "On Banner leave app", "Banner Ad events", "On Banner leave app", "On Banner leave app.", "onBannerLeaveApp");
AddCondition(240, cf_trigger, "On Banner dismiss", "Banner Ad events", "On Banner dismiss", "On Banner dismiss.", "onBannerDismiss");

AddCondition(250, cf_trigger, "On Interstitial Ad failed to receive", "Interstitial/Full screen Ad events", "On Interstitial Ad failed to receive", "On Interstitial Ad failed to receive.", "onInterstitialFailedToReceive");
AddCondition(260, cf_trigger, "On Interstitial Ad receive", "Interstitial/Full screen Ad events", "On Interstitial Ad receive", "On Interstitial Ad receive.", "onInterstitialReceive");
AddCondition(270, cf_trigger, "On Interstitial Ad present", "Interstitial/Full screen Ad events", "On Interstitial Ad present", "On Interstitial Ad present.", "onInterstitialPresent");
AddCondition(280, cf_trigger, "On Interstitial Ad leave app", "Interstitial/Full screen Ad events", "On Interstitial Ad leave app", "On Interstitial Ad leave app.", "onInterstitialLeaveApp");
AddCondition(290, cf_trigger, "On Interstitial Ad dismiss", "Interstitial/Full screen Ad events", "On Interstitial Ad dismiss", "On Interstitial Ad dismiss.", "onInterstitialDismiss");


////////////////////////////////////////
// Actions

// AddAction(id,				// any positive integer to uniquely identify this action
//			 flags,				// (see docs) af_none, af_deprecated
//			 list_name,			// appears in event wizard list
//			 category,			// category in event wizard list
//			 display_str,		// as appears in event sheet - use {0}, {1} for parameters and also <b></b>, <i></i>
//			 description,		// appears in event wizard dialog when selected
//			 script_name);		// corresponding runtime function name

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.");
AddStringParam("Android Ad Unit Id", "The Andoid Publisher Id.");
AddStringParam("iOS Unit Id", "The iOS Publisher Id.");
AddAction(0, af_none, "Create full screen Ad", "Interstitial/Full screen Ad", "Create a full screen Ad. Android Id <i>{1}</i>, iOS Id <i>{2}</i>. Tag: <b>{0}</b> ", "Create an Interstitial Ad.", "createFullScreenAd");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.");
AddAction(1, af_none, "Show full screen Ad", "Interstitial/Full screen Ad", "Show the full screen Ad. Tag: <b>{0}</b> ", "Show an Interstitial Ad.", "showFullScreenAd");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.");
AddStringParam("Android Ad Unit Id", "The Andoid Publisher Id.");
AddStringParam("iOS Unit Id", "The iOS Publisher Id.");
AddComboParamOption("Smart Banner - Auto fit");
AddComboParamOption("Banner");
AddComboParam("Banner Ad size", "The Ad size");

AddComboParamOption("No change");
AddComboParamOption("Top left");
AddComboParamOption("Top center");
AddComboParamOption("Top right");
AddComboParamOption("Left");
AddComboParamOption("Center");
AddComboParamOption("Right");
AddComboParamOption("Bottom left");
AddComboParamOption("Bottom center");
AddComboParamOption("Bottom right");
AddComboParam("Banner Ad position", "The Ad position");

AddComboParamOption("No");
AddComboParamOption("Yes");
AddComboParam("Banner Ad Overlap canvas", "The Ad will overlap canvas");

AddAction(110, af_none, "Create banner Ad", "Banner Ad", "Create a banner Ad. Android Id <i>{1}</i>, iOS Id <i>{2}</i>, size <i>{3}</i>. Tag: <b>{0}</b> ", "Create a banner Ad.", "createBannerAd");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different events.");
AddAction(120, af_none, "Show banner Ad", "Banner Ad", "Show banner Ad. Tag: <b>{0}</b>", "Show a banner Ad.", "showBannerAd");
AddAction(130, af_none, "Hide banner Ad", "Banner Ad", "Hide banner Ad", "Show a banner Ad.", "hideBannerAd");
AddAction(140, af_none, "Remove banner Ad", "Banner Ad", "Remove banner Ad", "Show a banner Ad.", "removeBannerAd");


////////////////////////////////////////
// Expressions

// AddExpression(id,			// any positive integer to uniquely identify this expression
//				 flags,			// (see docs) ef_none, ef_deprecated, ef_return_number, ef_return_string,
//								// ef_return_any, ef_variadic_parameters (one return flag must be specified)
//				 list_name,		// currently ignored, but set as if appeared in event wizard
//				 category,		// category in expressions panel
//				 exp_name,		// the expression name after the dot, e.g. "foo" for "myobject.foo" - also the runtime function name
//				 description);	// description in expressions panel

// Expressions here
AddExpression(0, ef_return_string, "Get last error", "Facebook Ads", "LastError", "Get the last error from Facebook Ads.");

////////////////////////////////////////
ACESDone();

////////////////////////////////////////
// Array of property grid properties for this plugin
// new cr.Property(ept_integer,		name,	initial_value,	description)		// an integer value
// new cr.Property(ept_float,		name,	initial_value,	description)		// a float value
// new cr.Property(ept_text,		name,	initial_value,	description)		// a string
// new cr.Property(ept_color,		name,	initial_value,	description)		// a color dropdown
// new cr.Property(ept_font,		name,	"Arial,-16", 	description)		// a font with the given face name and size
// new cr.Property(ept_combo,		name,	"Item 1",		description, "Item 1|Item 2|Item 3")	// a dropdown list (initial_value is string of initially selected item)
// new cr.Property(ept_link,		name,	link_text,		description, "firstonly")		// has no associated value; simply calls "OnPropertyChanged" on click

var property_list = [
    new cr.Property(ept_combo, "Test Ads", "Yes", "Set to Yes if testing the Ads", "Yes|No"),
    new cr.Property(ept_combo, "Auto show full screen Ad", "No", "Set to Yes to automatically show the interstitial Ad when it is ready", "Yes|No"),
    new cr.Property(ept_text, "Device Hash", "", "The device hash for test purposes", "")
];

// Called by IDE when a new object type is to be created
//noinspection JSUnusedGlobalSymbols
function CreateIDEObjectType() {
    return new IDEObjectType();
}

// Class representing an object type in the IDE
function IDEObjectType() {
    assert2(this instanceof arguments.callee, "Constructor called as a function");
}

// Called by IDE when a new object instance of this type is to be created
//noinspection JSUnusedGlobalSymbols
IDEObjectType.prototype.CreateInstance = function (instance) {
    return new IDEInstance(instance);
};

// Class representing an individual instance of an object in the IDE
function IDEInstance(instance, type) {
    assert2(this instanceof arguments.callee, "Constructor called as a function");

    // Save the constructor parameters
    //noinspection JSUnusedGlobalSymbols
    this.instance = instance;
    this.type = type;

    // Set the default property values from the property table
    this.properties = {};

    for (var i = 0; i < property_list.length; i++)
        this.properties[property_list[i].name] = property_list[i].initial_value;
}
// Called when inserted via Insert Object Dialog for the first time
//noinspection JSUnusedGlobalSymbols
IDEInstance.prototype.OnInserted = function () {
};

// Called after a property has been changed in the properties bar
//noinspection JSUnusedGlobalSymbols
IDEInstance.prototype.OnPropertyChanged = function (property_name) {
};

